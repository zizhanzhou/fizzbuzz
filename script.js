// Incremenet the value and set the result and current score
function incrementValue()
{
    let value = parseInt(document.getElementById('number').value, 10);
    value = isNaN(value) ? 0 : value;
    value++;
    document.getElementById('number').value = value;

    let message = ""
    if (value % 15 == 0) message = "FizzBuzz";
    else if (value % 3 == 0) message = "Fizz";
    else if (value % 5 == 0) message = "Buzz";
    else message = value.toString();
    console.log(message);

    localStorage["score"] = value;
    document.querySelector("#result").innerHTML = message;
    document.querySelector("#score").innerHTML = value;
}

// Get the user score history
const getData = async () => {
    const userId = localStorage["userId"];
    const response = await fetch(`http://basic-web.dev.avc.web.usf.edu/${userId}`);
    const myJson = await response.json(); //extract JSON from the http response

    console.log(myJson);
    document.querySelector("#pScore").innerHTML = JSON.stringify(myJson['score'], null, 4);

  }

// Update the user score
const updateScore = async () => {
    const userId = localStorage["userId"];
    const userScore = localStorage["score"];
    const response = await fetch(`http://basic-web.dev.avc.web.usf.edu/${userId}`, {
      method: 'POST',
      body: JSON.stringify({
        "score": userScore
    }), // string or object
      headers: {
        'Content-Type': 'application/json'
      }
    });
    const myJson = await response.json(); //extract JSON from the http response

}